package it.sedip.ticketing.application;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class SedipInizializer extends AbstractAnnotationConfigDispatcherServletInitializer{
	@Override
	 protected Class < ? > [] getRootConfigClasses() {
	  return new Class[] {
	   SedipConfiguration.class
	  };
	 }
	 @Override
	 protected Class < ? > [] getServletConfigClasses() {
	  return null;
	 }
	 @Override
	 protected String[] getServletMappings() {
	  return new String[] {"/rest/*","/web/*"};
	 }
}
