package it.sedip.ticketing.application;

import javax.sql.DataSource;

import it.sedip.ticketing.db.ValidateUserDao;
import it.sedip.ticketing.db.ValidateUserDaoImpl;
import it.sedip.ticketing.service.UserService;
import it.sedip.ticketing.service.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import it.sedip.ticketing.db.UserDao;
import it.sedip.ticketing.db.UserDaoImpl;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"it.sedip.ticketing.rest", "it.sedip.ticketing.webcontroller"})
public class SedipConfiguration {
	
	
	@Bean
	public DataSource getDataSource() {
		 DriverManagerDataSource ds = new DriverManagerDataSource();
		 ds.setDriverClassName(com.mysql.jdbc.Driver.class.getName());
		 ds.setUrl("jdbc:mysql://46.105.92.177:3306/ticketing?useSSL=true&serverTimezone=UTC");
		 ds.setUsername("ticketing");
		 ds.setPassword("Macbook123!");
		 return ds;
	}
	
	
	@Bean
    public UserDao getUserDAO() {
        return new UserDaoImpl(getDataSource());
    }

    @Bean
    public UserService getUserService() {return new UserServiceImpl();}

	@Bean
    public ValidateUserDao getValidateUserDAO(){return new ValidateUserDaoImpl(getDataSource());}
}
