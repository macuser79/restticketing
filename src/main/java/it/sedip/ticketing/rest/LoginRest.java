package it.sedip.ticketing.rest;

import it.sedip.ticketing.db.UserDao;
import it.sedip.ticketing.service.UserService;
import it.sedip.ticketing.to.UserTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.sedip.ticketing.response.UserResponse;

@RestController
@RequestMapping("/user")
public class LoginRest {
	
	private Logger logger = Logger.getLogger(LoginRest.class);

	@Autowired
	private UserDao userDAO;

	@Autowired
	private UserService userService;



	@RequestMapping(value = "/checkUser/{username}/{password}/{idSupplier}", method = RequestMethod.POST)
	public UserResponse checkLogin(@RequestHeader("language") final String language,
								   	@PathVariable final String username, @PathVariable final String password,
								    @PathVariable final Integer idSupplier) {
		 logger.info("Effettuo la login");
		try{
			UserTO userTO = new UserTO();
			userTO.setIdSupplier(new Integer(idSupplier));
			userTO.setPassword(password);
			userTO.setEmail(username);

			UserResponse userResponse = userService.checkLogin(userTO);
			return userResponse;
		}
		catch (Exception ex){

		}
		return null ;
	}
	
	@RequestMapping(value = "/registerUser/{username}/{password}/{name}/{surname}/{idSupplier}", method = RequestMethod.PUT)
	public UserResponse registerUser(@PathVariable final String username, @PathVariable final String password, @PathVariable final String name,
				@PathVariable final String surname, @PathVariable int idSupplier) {
		
		try{
			UserTO userTO = new UserTO();
			userTO.setEmail(username);
			userTO.setPassword(password);
			userTO.setName(name);
			userTO.setSurname(surname);
			userTO.setIdSupplier(idSupplier);
			UserResponse userResponse = userService.registerUser(userTO);
			return userResponse;
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
		
		
	}
	
	@RequestMapping(value="/changePassword/{newPassword}/{idCustomer}", method = RequestMethod.POST)
	public UserResponse changePassword(@PathVariable final String newPassword, @PathVariable final int idCustomer) {
		try{
			UserTO userTO = new UserTO();
			userTO.setPassword(newPassword);
			userTO.setIdCustomer(idCustomer);
			UserResponse userResponse = userService.resetPassword(userTO);
			return userResponse;
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
}
