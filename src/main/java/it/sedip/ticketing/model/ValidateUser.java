package it.sedip.ticketing.model;

import java.io.Serializable;

public class ValidateUser implements Serializable {

    private int idValidateCustomer;
    private String username;
    private int customer_IdCustomer;

    public int getIdValidateCustomer() {
        return idValidateCustomer;
    }

    public void setIdValidateCustomer(int idValidateCustomer) {
        this.idValidateCustomer = idValidateCustomer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCustomer_IdCustomer() {
        return customer_IdCustomer;
    }

    public void setCustomer_IdCustomer(int customer_IdCustomer) {
        this.customer_IdCustomer = customer_IdCustomer;
    }
}
