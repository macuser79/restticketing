package it.sedip.ticketing.db;

import java.util.List;

import it.sedip.ticketing.exception.TicketingException;
import it.sedip.ticketing.model.User;
import it.sedip.ticketing.to.UserTO;

public interface UserDao {
	
	public void saveOrUpdate(User user);
    
    public void delete(int idUser);
     
    public User get(int idUser);
     
    public List<User> list();
    
    public User registerUser(User user) throws TicketingException;


    public User checkUserByEmail(String username, Integer idSupplier) throws TicketingException;


    public User loginUser(UserTO userTO) throws TicketingException;

    public boolean updatePassword(UserTO userTO) throws TicketingException;

    public boolean enableUser(int idCustomer) throws TicketingException;

}
