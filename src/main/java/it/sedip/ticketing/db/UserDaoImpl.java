package it.sedip.ticketing.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import it.sedip.ticketing.model.User;
import it.sedip.ticketing.to.UserTO;
import it.sedip.ticketing.utility.SendEmail;
import it.sedip.ticketing.utility.Utility;
import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import it.sedip.ticketing.exception.TicketingException;


@Repository
public class UserDaoImpl implements UserDao {

    private JdbcTemplate jdbcTemplate;

    private Logger logger = Logger.getLogger(UserDaoImpl.class);


    private String sqlRegisterUser = "INSERT INTO customer (Name, Surname, Password, Email, Supplier_idSupplier) "
            + "VALUES (?,?,?,?,?)";

    private String sqlValidateUser = "INSERT INTO validatecustomer (UserName, Customer_idCustomer) VALUES (?,?)";

    private String sqlCheckUser = "SELECT * FROM customer WHERE Email = ? AND Supplier_idSupplier = ?";


    private String sqlLoginUser = "SELECT * FROM customer WHERE Email = ? AND Password = ? AND Supplier_idSupplier = ?";

    private String updatePassword = "UPDATE customer SET Password = ? WHERE Supplier_idSupplier = ?";

    private String enableUser = "UPDATE customer SET Active = 1 where idCustomer = ?";


    public UserDaoImpl(DataSource dataSource) {
        // TODO Auto-generated constructor stub
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void saveOrUpdate(User user) {

    }

    @Override
    public void delete(int idUser) {
        // TODO Auto-generated method stub

    }

    @Override
    public User get(int idUser) {

        return null;
    }

    @Override
    public List<User> list() {
        // TODO Auto-generated method stub
        return null;
    }




    @Override
    public User registerUser(User user) throws TicketingException {

        KeyHolder keyHolder = new GeneratedKeyHolder();
        try {

            //Effettuo l'inserimento all'interno della tabella Customer
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement(sqlRegisterUser, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, user.getName());
                    ps.setString(2, user.getSurname());
                    ps.setString(3, user.getPassword());
                    ps.setString(4, user.getEmail());
                    ps.setInt(5, user.getIdSupplier());
                    return ps;
                }
            }, keyHolder);

            //Effettuo l'hash dello username;
            String sha1String = Utility.convertStringtoSha1(user.getEmail());
            logger.debug("Valore SHA1: " + sha1String);

            logger.debug("Valore key: " + keyHolder.getKey().intValue());

            //Effettuo l'inserimento all'interno della tabella ValidateUser
            jdbcTemplate.update(sqlValidateUser, sha1String, keyHolder.getKey().intValue());

            logger.debug("Valore key: " + keyHolder.getKey().intValue());


            //Invio l'email all'utente finale
            boolean sendEmail = SendEmail.sendEmail(user);
            if(sendEmail){
                user.setIdCustomer(keyHolder.getKey().intValue());
                user.setActive(0);
                return user;
            }
            return null;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public User checkUserByEmail(String username, Integer idSupplier) throws TicketingException {

        try{
            return (User) jdbcTemplate.queryForObject(
                    sqlCheckUser,
                    new Object[]{username, idSupplier},
                    new BeanPropertyRowMapper<>(User.class)
            );
        }
        catch (EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public User loginUser(UserTO userTO) throws TicketingException {
        try{
            return (User) jdbcTemplate.queryForObject(
                    sqlLoginUser,
                    new Object[]{userTO.getEmail(), userTO.getPassword(), userTO.getIdSupplier()},
                    new BeanPropertyRowMapper<>(User.class)
            );
        }
        catch (EmptyResultDataAccessException ex){
            return null;
        }
    }

    @Override
    public boolean updatePassword(UserTO userTO) throws TicketingException {

        try{
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement(updatePassword, Statement.NO_GENERATED_KEYS);
                    ps.setString(1, userTO.getPassword());
                    ps.setInt(2, userTO.getIdSupplier());
                    return ps;
                }
            });
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }

    @Override
    public boolean enableUser(int idCustomer) throws TicketingException {
        try{
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement(enableUser, Statement.NO_GENERATED_KEYS);
                    ps.setInt(1, idCustomer);
                    return ps;
                }
            });
            return true;
        }
        catch (Exception ex){
            return false;
        }
    }


}
