package it.sedip.ticketing.db;

import it.sedip.ticketing.exception.TicketingException;
import it.sedip.ticketing.model.ValidateUser;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class ValidateUserDaoImpl implements ValidateUserDao{

    private JdbcTemplate jdbcTemplate;


    private String sqlGetValidateUser = "SELECT * FROM validatecustomer WHERE UserName = ? ";


    public ValidateUserDaoImpl(DataSource dataSource){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public ValidateUser getValidateUser(String username) throws TicketingException {
        try{
            return (ValidateUser) jdbcTemplate.queryForObject(
                    sqlGetValidateUser,
                    new Object[]{username},
                    new BeanPropertyRowMapper<>(ValidateUser.class)
            );
        }
        catch (EmptyResultDataAccessException ex){
            return null;
        }
    }
}
