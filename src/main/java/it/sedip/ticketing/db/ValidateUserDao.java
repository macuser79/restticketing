package it.sedip.ticketing.db;

import it.sedip.ticketing.exception.TicketingException;
import it.sedip.ticketing.model.ValidateUser;

public interface ValidateUserDao {

    public ValidateUser getValidateUser(String username) throws TicketingException;

}
