package it.sedip.ticketing.exception;

public class TicketingException extends Exception{
	
	public TicketingException(String message) {
		super(message);
	}

}
