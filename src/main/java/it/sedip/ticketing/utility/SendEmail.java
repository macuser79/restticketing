package it.sedip.ticketing.utility;

import it.sedip.ticketing.model.User;
import it.sedip.ticketing.exception.TicketingException;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {



    public static final boolean sendEmail(User user) throws TicketingException {

        String username = "ricetteveganeapp@gmail.com";
        String password = "Macbook123!";

        try{
            Properties prop = new Properties();
    		prop.put("mail.smtp.host", "smtp.gmail.com");
            prop.put("mail.smtp.port", "587");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true"); //TLS

            Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
            });
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ricetteveganeapp@gmail.com"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(user.getEmail())
            );
            message.setSubject("Attivazione utente Ticketing");

            String sMessage = "Ciao " + user.getName() + " " + user.getSurname();
            sMessage = sMessage + "\n\n";
            sMessage = sMessage + "Grazie per aver scelto di utilizzare i servizi Sedip.";
            sMessage = sMessage + "\n\n";
            sMessage = sMessage + "per poter accedere e' necessario confermare il tuo account tramite il seguente link:";
            sMessage = sMessage + "\n\n";

            String sha1Email = Utility.convertStringtoSha1(user.getEmail());
            sMessage = sMessage + "http://192.168.178.22:8080/RestTicketing/web/validateUser/"+sha1Email;

            message.setText(sMessage);
            Transport.send(message);

            return true;
        }
        catch (Exception ex){
            throw new TicketingException(ex.getMessage());
        }
    }
}
