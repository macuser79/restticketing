package it.sedip.ticketing.utility;

public interface TicketingMessages {
	
	public static final String MESSAGE_OK = "OK";
	public static final Integer CODE_OK = 0;

	public static final String MESSAGE_KO = "KO";
	public static final Integer ERROR_CODE_REGISTER = -100;

	public static final String MESSAGE_USER_PRESENT_NO_ACTIVE = "MESSAGE_USER_PRESENT_NO_ACTIVE";
	public static final Integer ERROR_CODE_USER_PRESENT_NO_ACTIVE = -101;

	public static final String MESSAGE_USER_PRESENT = "MESSAGE_USER_PRESENT";
	public static final Integer ERROR_CODE_USER_PRESENT = -102;


	public static final String MESSAGE_USER_NO_ACTIVE = "MESSAGE_USER_NO_ACTIVE";
	public static final Integer ERROR_CODE_USER_NO_ACTIVE = -103;

	public static final String MESSAGE_USER_NO_VALID_LOGIN = "MESSAGE_USER_NO_VALID_LOGIN";
	public static final Integer ERROR_CODE_USER_NO_VALID_LOGIN = -104;


	public static final Integer ERROR_GENERIC_CODE = -999;



}
