package it.sedip.ticketing.utility;

import it.sedip.ticketing.exception.TicketingException;

import java.security.MessageDigest;

public class Utility {

    public static String convertStringtoSha1(String input) throws TicketingException {

        try{
            MessageDigest mDigest = MessageDigest.getInstance("SHA1");
            byte[] result = mDigest.digest(input.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < result.length; i++) {
                sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        }
        catch(Exception ex){
            throw new TicketingException(ex.getMessage());
        }



    }

}
