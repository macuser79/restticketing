package it.sedip.ticketing.service;

import it.sedip.ticketing.db.ValidateUserDao;
import it.sedip.ticketing.model.User;
import it.sedip.ticketing.db.UserDao;
import it.sedip.ticketing.exception.TicketingException;
import it.sedip.ticketing.model.ValidateUser;
import it.sedip.ticketing.response.UserResponse;
import it.sedip.ticketing.to.UserTO;
import it.sedip.ticketing.utility.TicketingMessages;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceImpl implements UserService{

    private Logger logger = Logger.getLogger(UserServiceImpl.class);


    @Autowired
    private UserDao userDAO;

    @Autowired
    private ValidateUserDao validateUserDao;

    @Override
    public UserResponse registerUser(UserTO userTO) throws TicketingException {

        try{
            //Verifico che l'utente non si sia gia' registrato
            User checkUser = userDAO.checkUserByEmail(userTO.getEmail(), userTO.getIdSupplier());
            if(checkUser!=null){
                //L'utente è già presente e non faccio la registrazione ma verifico se l'utente e' attivo oppure no
                //questo mi serve per verificare lo stato dell'utente e fornire le informazioni necessarie

                logger.debug(checkUser.getEmail());
                if(checkUser.getActive() == 0){
                    //Utente presente ma non attivo
                    UserResponse userResponse = this.toUserResponse(checkUser);
                    userResponse.setCode(TicketingMessages.ERROR_CODE_USER_PRESENT_NO_ACTIVE);
                    userResponse.setStatus(TicketingMessages.MESSAGE_USER_PRESENT_NO_ACTIVE);
                    return userResponse;
                }
                //Utente presente e attivo
                UserResponse userResponse = this.toUserResponse(checkUser);
                userResponse.setCode(TicketingMessages.ERROR_CODE_USER_PRESENT);
                userResponse.setStatus(TicketingMessages.MESSAGE_USER_PRESENT);
                return userResponse;
            }
            //Utente non presente e procedo con la sua registrazione
            User userInput = this.toUserDb(userTO);
            User user = userDAO.registerUser(userInput);
            UserResponse userResponse = this.toUserResponse(user);
            userResponse.setCode(TicketingMessages.CODE_OK);
            userResponse.setStatus(TicketingMessages.MESSAGE_OK);
            return userResponse;
        }
        catch (Exception ex){
            throw new TicketingException(ex.getMessage());
        }
    }

    @Override
    public UserResponse checkLogin(UserTO userTO) throws TicketingException {
        try{
            //Effettuo la login con le credenziali fornite;
            User checkUser = userDAO.loginUser(userTO);


            //Verifico se l'utente e' attivo oppure no
            if(checkUser.getActive() == 0){
                //Utente presente ma non attivo
                UserResponse userResponse = this.toUserResponse(checkUser);
                userResponse.setCode(TicketingMessages.ERROR_CODE_USER_PRESENT_NO_ACTIVE);
                userResponse.setStatus(TicketingMessages.MESSAGE_USER_PRESENT_NO_ACTIVE);
                return userResponse;
            }



            UserResponse userResponse = toUserResponse(checkUser);
            userResponse.setCode(TicketingMessages.CODE_OK);
            userResponse.setStatus(TicketingMessages.MESSAGE_OK);
            return userResponse;

        }
        catch(Exception ex){
            throw new TicketingException(ex.getMessage());
        }
    }

    @Override
    public UserResponse resetPassword(UserTO userTO) throws TicketingException {
        try{
            UserResponse userResponse = new UserResponse();
            boolean bContinue = userDAO.updatePassword(userTO);
            if(bContinue){

                userResponse.setCode(TicketingMessages.CODE_OK);
                userResponse.setStatus(TicketingMessages.MESSAGE_OK);
                return userResponse;
            }
            userResponse.setCode(TicketingMessages.ERROR_GENERIC_CODE);
            userResponse.setStatus(TicketingMessages.MESSAGE_OK);
            return userResponse;
        }
        catch (Exception ex){
            throw new TicketingException(ex.getMessage());
        }
    }

    @Override
    public boolean validateUser(String email) throws TicketingException {
        try{

            ValidateUser validateUser = validateUserDao.getValidateUser(email);
            if(validateUser!=null){
                return userDAO.enableUser(validateUser.getCustomer_IdCustomer());
            }
        }
        catch (Exception ex){
            throw new TicketingException(ex.getMessage());
        }
        return false;
    }


    private User toUserDb(UserTO userTO) {

        User user = new User();
        BeanUtils.copyProperties(userTO, user);
        return user;

    }

    private UserResponse toUserResponse(User user) {
        UserResponse userResponse = new UserResponse();
        BeanUtils.copyProperties(user, userResponse);
        return userResponse;
    }


}
