package it.sedip.ticketing.service;

import it.sedip.ticketing.exception.TicketingException;
import it.sedip.ticketing.response.UserResponse;
import it.sedip.ticketing.to.UserTO;

public interface UserService {

    public UserResponse registerUser(UserTO userTO) throws TicketingException;


    public UserResponse checkLogin(UserTO userTO) throws TicketingException;

    public UserResponse resetPassword(UserTO userTO) throws TicketingException;

    public boolean validateUser(String email) throws TicketingException;
}
