package it.sedip.ticketing.webcontroller;

import it.sedip.ticketing.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ValidateUserController {

    private Logger logger = Logger.getLogger(ValidateUserController.class);

    //http://localhost:8080/RestTicketing/web/validateUser/pippo

    @Autowired
    private UserService userService;




    @RequestMapping(value = "/validateUser/{username}", method = RequestMethod.GET)
    public boolean enableUser(@PathVariable String username){

        try{
            boolean bContinue = userService.validateUser(username);
            if(bContinue){
                logger.info("VALIDAZIONE UTENTE EFFETTUATA CON SUCCESSO");
            }
        }
        catch(Exception ex){

        }
        return true;
    }
}
