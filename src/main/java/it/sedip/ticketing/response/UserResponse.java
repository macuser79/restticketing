package it.sedip.ticketing.response;

public class UserResponse extends BaseResponse{

	
	private String email;
	private String password;
	private String name;
	private String surname;
	private Integer active;
	private Integer idCustomer;
	
	
	
	public UserResponse() {
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getActive() {
		return active;
	}




	public void setActive(Integer active) {
		this.active = active;
	}


	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	@Override
	public String toString() {
		return "UserResponse{" +
				"email='" + email + '\'' +
				", password='" + password + '\'' +
				", name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", active=" + active +
				'}';
	}
}
