-- MySQL Workbench Synchronization
-- Generated: 2020-05-18 17:01
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: c.petrucci

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `ticketing`.`Customer` (
  `idCustomer` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Surname` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(100) NOT NULL,
  `E-mail` VARCHAR(45) NOT NULL,
  `Active` TINYINT(4) NOT NULL DEFAULT 0 COMMENT 'campo che indica se l\'utente è attivo o meno',
  `Supplier_idSupplier` INT(11) NOT NULL,
  PRIMARY KEY (`idCustomer`),
  INDEX `fk_Customer_Supplier_idx` (`Supplier_idSupplier` ASC),
  CONSTRAINT `fk_Customer_Supplier`
    FOREIGN KEY (`Supplier_idSupplier`)
    REFERENCES `ticketing`.`Supplier` (`idSupplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`Order` (
  `idOrder` INT(11) NOT NULL AUTO_INCREMENT,
  `OrderNumber` VARCHAR(45) NULL DEFAULT NULL,
  `OrderDate` DATETIME NOT NULL,
  `TotalAmount` DECIMAL(12,2) NOT NULL,
  `Customer_idCustomer` INT(11) NOT NULL,
  PRIMARY KEY (`idOrder`),
  INDEX `fk_Order_Customer_idx` (`Customer_idCustomer` ASC),
  CONSTRAINT `fk_Order_Customer`
    FOREIGN KEY (`Customer_idCustomer`)
    REFERENCES `ticketing`.`Customer` (`idCustomer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`OrderItem` (
  `Order_idOrder` INT(11) NOT NULL,
  `ProductCredit_idProductCredit` INT(11) NOT NULL,
  `QrCode` VARCHAR(100) NOT NULL,
  `QrText` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`Order_idOrder`, `ProductCredit_idProductCredit`),
  INDEX `fk_OrderItem_ProductCredit_idx` (`ProductCredit_idProductCredit` ASC),
  CONSTRAINT `fk_OrderItem_Order`
    FOREIGN KEY (`Order_idOrder`)
    REFERENCES `ticketing`.`Order` (`idOrder`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OrderItem_ProductCredit`
    FOREIGN KEY (`ProductCredit_idProductCredit`)
    REFERENCES `ticketing`.`ProductCredit` (`idProductCredit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`Supplier` (
  `idSupplier` INT(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` VARCHAR(50) NOT NULL,
  `Phone` VARCHAR(15) NOT NULL,
  `E-mail` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idSupplier`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`SupplierCredit` (
  `idSupplierCredit` INT(11) NOT NULL AUTO_INCREMENT,
  `Code` VARCHAR(45) NOT NULL,
  `Quantity` INT(11) NOT NULL,
  `Aggio` DECIMAL(12,2) NOT NULL,
  `TotalAmount` DECIMAL(12,2) NOT NULL,
  `Supplier_idSupplier` INT(11) NOT NULL,
  PRIMARY KEY (`idSupplierCredit`),
  INDEX `fk_SupplierCredit_Supplier_idx` (`Supplier_idSupplier` ASC),
  CONSTRAINT `fk_SupplierCredit_Supplier`
    FOREIGN KEY (`Supplier_idSupplier`)
    REFERENCES `ticketing`.`Supplier` (`idSupplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`ProductCredit` (
  `idProductCredit` INT(11) NOT NULL AUTO_INCREMENT,
  `idProduct` INT(11) NOT NULL,
  `Quantity` INT(11) NOT NULL,
  `SupplierCredit_idSupplierCredit` INT(11) NOT NULL,
  INDEX `fk_ProductCredit_SupplierCredit_idx` (`SupplierCredit_idSupplierCredit` ASC) ,
  PRIMARY KEY (`idProductCredit`),
  CONSTRAINT `fk_ProductCredit_SupplierCredit`
    FOREIGN KEY (`SupplierCredit_idSupplierCredit`)
    REFERENCES `ticketing`.`SupplierCredit` (`idSupplierCredit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`Product` (
  `idProduct` INT(11) NOT NULL AUTO_INCREMENT,
  `ProductName` VARCHAR(100) NOT NULL,
  `UnitPrice` DECIMAL(12,2) NOT NULL,
  `ProductCredit_idProductCredit` INT(11) NOT NULL,
  PRIMARY KEY (`idProduct`),
  INDEX `fk_Product_ProductCredit_idx` (`ProductCredit_idProductCredit` ASC),
  CONSTRAINT `fk_Product_ProductCredit`
    FOREIGN KEY (`ProductCredit_idProductCredit`)
    REFERENCES `ticketing`.`ProductCredit` (`idProductCredit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`ValidateCustomer` (
  `idValidateCustomer` INT(11) NOT NULL AUTO_INCREMENT,
  `UserName` VARCHAR(50) NOT NULL,
  `Customer_idCustomer` INT(11) NOT NULL,
  PRIMARY KEY (`idValidateCustomer`),
  INDEX `fk_ValidateCustomer_Customer_idx` (`Customer_idCustomer` ASC),
  CONSTRAINT `fk_ValidateCustomer_Customer`
    FOREIGN KEY (`Customer_idCustomer`)
    REFERENCES `ticketing`.`Customer` (`idCustomer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`LogCustomer` (
  `idLogCustomer` INT(11) NOT NULL AUTO_INCREMENT,
  `LoginDate` DATETIME NOT NULL,
  `Function` VARCHAR(100) NULL DEFAULT NULL,
  `ErrorDescription` VARCHAR(5000) NULL DEFAULT NULL,
  `Customer_idCustomer` INT(11) NOT NULL,
  PRIMARY KEY (`idLogCustomer`),
  INDEX `fk_LogCustomer_Customer_idx` (`Customer_idCustomer` ASC),
  CONSTRAINT `fk_LogCustomer_Customer`
    FOREIGN KEY (`Customer_idCustomer`)
    REFERENCES `ticketing`.`Customer` (`idCustomer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ticketing`.`CustomerCredit` (
  `idCustomerCredit` INT(11) NOT NULL AUTO_INCREMENT,
  `Code` VARCHAR(45) NOT NULL,
  `TotalAmount` DECIMAL(12,2) NOT NULL,
  `Customer_idCustomer` INT(11) NOT NULL,
  `SupplierCredit_idSupplierCredit` INT(11) NOT NULL,
  PRIMARY KEY (`idCustomerCredit`),
  INDEX `fk_CustomerCredit_Customer_idx` (`Customer_idCustomer` ASC) ,
  INDEX `fk_CustomerCredit_SupplierCredit_idx` (`SupplierCredit_idSupplierCredit` ASC) ,
  CONSTRAINT `fk_CustomerCredit_Customer`
    FOREIGN KEY (`Customer_idCustomer`)
    REFERENCES `ticketing`.`Customer` (`idCustomer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CustomerCredit_SupplierCredit`
    FOREIGN KEY (`SupplierCredit_idSupplierCredit`)
    REFERENCES `ticketing`.`SupplierCredit` (`idSupplierCredit`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
